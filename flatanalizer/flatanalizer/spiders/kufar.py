import json

import scrapy

from flatanalizer.items.kufar import KufarItem, KufarResponse


class KufarSpider(scrapy.Spider):
    name = 'kufar'

    def _get_kufar_url(self, cursor=None, page_size=100):
        url = ('https://re.kufar.by/'
               'api/search/ads-search/v1/engine/v1/search/raw-paginated'
               '?sort=lst.d'
               f'&size={page_size}'
               '&typ=let'
               '&prn=1000')
        if cursor is not None:
            url += f'&cursor={cursor}'
        return url

    def start_requests(self):
        url = self._get_kufar_url()
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        data = json.loads(response.text)
        kufar_response = KufarResponse(**data)
        yield KufarItem(response=kufar_response)

        try:
            next_page = [
                page
                for page in kufar_response.pagination.pages
                if page.label == 'next'][0]
        except IndexError:
            return

        if next_page.token is not None:
            next_page = self._get_kufar_url(cursor=next_page.token)
            yield response.follow(next_page, self.parse)
