from locust import HttpLocust, TaskSet, between


def translate(l):
    l.client.post("/translate", {"word": "hi"}, headers={
        'x-api-key': 'AKIAYRDTOJDNU7UNAY7G',
        'Accept': 'application/json',
    })


class UserBehavior(TaskSet):
    def on_start(self):
        translate(self)


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(5.0, 9.0)
