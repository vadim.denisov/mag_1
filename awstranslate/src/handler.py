import json
import logging

logger = logging.getLogger('aws_xray_sdk')
logger.setLevel(logging.DEBUG)


def _read_vocab(path):
    with open(path) as f_vocab:
        return json.load(f_vocab)


def _parse_event(event):
    request = json.loads(event['body'])
    if 'word' not in request:
        raise Exception(r'Invalid Request body. Valid: { "word": "hello" }')
    return request['word']


def translate(en_word, vocab):
    if en_word not in vocab:
        raise Exception(r'Word not in vocab')
    return vocab[en_word]


def main(event, context):
    logger.info(event)
    response = {'statusCode': 400, 'body': ''}
    logger.debug('This is a debug message')
    logger.info('This is an info message')
    logger.warning('This is a warning message')
    logger.error('This is an error message')
    logger.critical('This is a critical message')

    try:
        vocab = _read_vocab('data/vocab.json')
        en_word = _parse_event(event)
        ru_word = translate(en_word, vocab)

        response = {'statusCode': 200, 'body': ru_word}
    except Exception as error:
        logger.error(error)
        response = {'statusCode': 200, 'body': str(error)}

    return response


{'resource': '/translate',
    'path': '/translate',
    'httpMethod': 'POST',
    'headers': None,
    'multiValueHeaders': None,
    'queryStringParameters': None,
    'multiValueQueryStringParameters': None,
    'pathParameters': None,
    'stageVariables': None,

    'body': '{"word": "hello"}',
 'isBase64Encoded': False}
