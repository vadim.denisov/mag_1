import numpy as np


def find_nash_equilibrium(game_matrix):
    """
    game_matrix dimensions: [
      | strategy # for player 1,
      | strategy # for player 2,
      | ...,
      | strategy # for player N,
      | reward for player #]
    Returns a set of nash equilibrium points.
    """
    players_number = game_matrix.ndim - 1
    assert players_number > 1
    assert game_matrix.shape[-1] == players_number
    best_players_strategies = []
    for player in range(players_number):
        player_matrix = game_matrix[..., player]
        max_rewards = player_matrix.max(axis=player, keepdims=True)
        best_player_strategies = np.argwhere(player_matrix == max_rewards)
        best_players_strategies.append(set(tuple(map(tuple, best_player_strategies))))
    return set.intersection(*best_players_strategies)
