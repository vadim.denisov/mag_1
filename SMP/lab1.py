import numpy as np
import scipy.optimize as optimize


class AlgorithmError(Exception):
    def __init__(self, details):
        self.details = details


def solve_task_distribution(workers_num, tasks_time, tasks_workers):
    """
    Workers and tasks are enumerated starting from 0.
    tasks_time[i] - time to perform task#i.
    tasks_workers[i] - a list containing workers' ids that can do task#i; workers' ids mustn't repeat.

    Linear programming variables order:
     | Time spent by worker#tasks_workers[0, 0] on task#0.
     | Time spent by worker#tasks_workers[0, 1] on task#0.
     | ...
     | Time spent by worker#tasks_workers[0, -1] on task#0.
     | Time spent by worker#tasks_workers[1, 0] on task#1.
     | ...
     | Time spent by worker#tasks_workers[-1, -1] on task#(len(tasks_time) - 1).
     | Time needed to perform all tasks.

    Throws AlgorithmError on error.

    Returns a tuple consisting of two values:
     1. Time spent by workers on tasks in the same format as and according to the tasks_workers parameter.
     2. Time needed to perform all tasks.
    """

    tasks_num = len(tasks_time)
    assert workers_num > 0 and tasks_num > 0
    assert len(tasks_workers) == tasks_num

    assert all(map(
        lambda task_workers: task_workers and len(set(task_workers)) == len(task_workers),
        tasks_workers))

    assert all(map(
        lambda task_workers: all(map(lambda task_worker: 0 <= task_worker < workers_num, task_workers)),
        tasks_workers))

    tasks_workers_array = [np.array(task_workers, dtype=int) for task_workers in tasks_workers]

    # noinspection PyTypeChecker
    # [tasks_index[i], tasks_index[i + 1]) - indices of variables that denote time spent on task#i
    # tasks_index[-1] - the index of the variable that contains time needed to perform all tasks
    tasks_index = np.cumsum([0 if i == 0 else tasks_workers_array[i - 1].size for i in range(tasks_num + 1)])
    variables_num = tasks_index[-1] + 1

    # workers_index[i] - a list of indices of variables that denote time spent by worker#i
    workers_index = [
        np.array(
            [task_workers_index + tasks_index[task]
             for task in range(tasks_num)
             for task_workers_index in np.argwhere(tasks_workers_array[task] == worker)],
            dtype=int)
        for worker in range(workers_num)]

    obj_func_vector = np.zeros((variables_num,))
    obj_func_vector[-1] = 1

    upper_bound_matrix = np.zeros((workers_num, variables_num))
    upper_bound_matrix[:, -1] = -1

    for worker, worker_index in enumerate(workers_index):
        upper_bound_matrix[worker, worker_index] = 1

    upper_bound_vector = np.zeros((workers_num,))

    equality_matrix = np.array([
        [1 if tasks_index[i] <= j < tasks_index[i + 1] else 0 for j in range(variables_num)]
        for i in range(tasks_num)])

    equality_vector = np.array(tasks_time)

    # noinspection PyTypeChecker
    linprog_result = optimize.linprog(
        obj_func_vector, upper_bound_matrix, upper_bound_vector, equality_matrix, equality_vector)

    if not linprog_result.success:
        raise AlgorithmError(linprog_result)

    tasks_workers_time = [
        linprog_result.x[tasks_index[task]:tasks_index[task + 1]].tolist()
        for task in range(tasks_num)]

    return tasks_workers_time, linprog_result.fun
