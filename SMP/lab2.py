import numpy as np
from scipy import linalg
from scipy import optimize


class AlgorithmError(Exception):
    def __init__(self, details):
        self.details = details


# noinspection PyPep8Naming
def delayed_column_generation(A1, A2, B1, B2, c1, c2, b0, b1, b2, Ab, db, eps=1e-10):
    """
    All variables are expected to be of type `numpy.ndarray` with `float` element types.
    Returns `None` if the object function is unbounded on the set of permissible plans.
    Otherwise, returns the optimal object function value.
    Throws `AlgorithmError` on error.
    """

    # noinspection PyPep8Naming
    def solve_subtask(A, B, b, c, uf, ul):
        """Returns (solution, obj func value)."""
        obj_func_v = uf @ A - c
        linprog_res = optimize.linprog(obj_func_v, A_eq=B, b_eq=b)
        if linprog_res.success:
            return linprog_res.x, linprog_res.fun + ul
        else:
            raise AlgorithmError(linprog_res)

    # noinspection PyPep8Naming
    def find_new_components(A, Anew_end, c, xp):
        """Returns (Anew, d_new)."""
        return np.append(A @ xp, Anew_end), c @ xp

    assert A1.ndim == A2.ndim == B1.ndim == B2.ndim == Ab.ndim == 2
    assert c1.ndim == c2.ndim == b0.ndim == db.ndim == 1
    assert A1.shape[0] == A2.shape[0] == b0.shape[0] == Ab.shape[0] - 2 == Ab.shape[1] - 2 == db.shape[0] - 2
    assert A1.shape[1] == B1.shape[1] == c1.shape[0]
    assert A2.shape[1] == B2.shape[1] == c2.shape[0]
    assert B1.shape[0] == b1.shape[0]
    assert B2.shape[0] == b2.shape[0]
    be = np.append(b0, [1, 1])

    while True:
        # Step 1
        Abi = linalg.inv(Ab)

        # Step 2
        u = (db @ Abi)
        xp1, delta1 = solve_subtask(A1, B1, b1, c1, u[:-2], u[-2])
        if delta1 >= -eps:
            xp2, delta2 = solve_subtask(A2, B2, b2, c2, u[:-2], u[-1])
            if delta2 >= -eps:
                # The optimal solution is found
                return u @ be

        # Step 3
        if delta1 < -eps:
            Anew, d_new = find_new_components(A1, np.array([1, 0]), c1, xp1)
        else:
            # noinspection PyUnboundLocalVariable
            Anew, d_new = find_new_components(A2, np.array([0, 1]), c2, xp2)

        # Step 4
        z = Abi @ Anew
        z_pos_ind = np.argwhere(z > eps)
        if z_pos_ind.size == 0:
            # The object function is unbounded on the set of permissible plans
            return None
        theta = Abi[z_pos_ind] @ be / z[z_pos_ind]
        min_theta = np.argmin(theta)
        replacing_column = z_pos_ind[min_theta][0]

        # Step 5
        Ab[:, replacing_column] = Anew
        db[replacing_column] = d_new
